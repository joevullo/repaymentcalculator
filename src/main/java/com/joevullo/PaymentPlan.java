package com.joevullo;

import java.math.BigDecimal;

public class PaymentPlan {
    private BigDecimal regularAmount;
    private BigDecimal lastAmount;

    public PaymentPlan(BigDecimal regularAmount, BigDecimal lastAmount) {
        this.regularAmount = regularAmount;
        this.lastAmount = lastAmount;
    }

    public BigDecimal getRegularAmount() {
        return regularAmount;
    }

    public BigDecimal getLastAmount() {
        return lastAmount;
    }

    public boolean isLastAmountDifferent() {
        return !regularAmount.equals(lastAmount);
    }
}
