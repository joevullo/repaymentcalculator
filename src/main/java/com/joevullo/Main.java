package com.joevullo;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner reader = new Scanner(System.in);
        System.out.println("Enter total payment: ");
        double payment = reader.nextDouble();

        System.out.println("Enter number of payments: ");
        int numberOfPayments = reader.nextInt();

        PaymentPlan paymentPlan = PaymentPlanCalculator.calculate(payment, numberOfPayments);

        System.out.println("Regular Amount: " + paymentPlan.getRegularAmount());

        if (paymentPlan.isLastAmountDifferent())
            System.out.println("Last Amount: " + paymentPlan.getLastAmount());

        reader.close();
    }
}
