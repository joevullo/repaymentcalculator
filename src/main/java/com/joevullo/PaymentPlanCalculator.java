package com.joevullo;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class PaymentPlanCalculator {

    private final static Integer CURRENCY_PRECISION = 2;
    private final static Integer DEFAULT_MAXIMUM_PRECISION = 20;

    public static PaymentPlan calculate(Double totalAmount, int numberOfPayments) {
        BigDecimal numberOfPaymentsBigDecimal = new BigDecimal(numberOfPayments);

        BigDecimal paymentValid = new BigDecimal(totalAmount).divide(numberOfPaymentsBigDecimal, CURRENCY_PRECISION, RoundingMode.DOWN);

        BigDecimal paymentWithFullScale = new BigDecimal(totalAmount).divide(numberOfPaymentsBigDecimal, DEFAULT_MAXIMUM_PRECISION, RoundingMode.HALF_UP);

        BigDecimal leftOverValue = paymentWithFullScale.subtract(paymentValid).multiply(numberOfPaymentsBigDecimal);

        BigDecimal finalPaymentIncludingLeftOver = paymentValid.add(leftOverValue).setScale(CURRENCY_PRECISION, RoundingMode.HALF_UP);

        return new PaymentPlan(paymentValid, finalPaymentIncludingLeftOver);
    }
}
