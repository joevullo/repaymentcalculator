package com.joevullo;

import static org.junit.Assert.*;

import org.junit.Test;

import java.math.BigDecimal;
import java.math.RoundingMode;

/**
 * You'll have to add junit to classpath in IDE or simply delete this file.
 */
public class PaymentPlanCalculatorTest {

    private final static Integer CURRENCY_PRECISION = 2;
    private final static RoundingMode DEFAULT_ROUNDING_MODE = RoundingMode.HALF_UP;

    @Test
    public void whenPaymentCalculatorReceivesEasilyDivisibleValues_ThenResultIsCorrect() {
        BigDecimal expectedValue = new BigDecimal(2.50).setScale(CURRENCY_PRECISION, DEFAULT_ROUNDING_MODE);

        PaymentPlan paymentPlan = PaymentPlanCalculator.calculate(10.00, 4);
        assertEquals(expectedValue, paymentPlan.getRegularAmount());
        assertEquals(expectedValue, paymentPlan.getLastAmount());
        assertFalse(paymentPlan.isLastAmountDifferent());
    }

    @Test
    public void whenPaymentDoesNotDivideNicely_ThenCalculatorWorksOutValuesCorrectly() {
        PaymentPlan paymentPlan = PaymentPlanCalculator.calculate(10.00, 3);
        assertEquals(new BigDecimal(3.33).setScale(CURRENCY_PRECISION, DEFAULT_ROUNDING_MODE), paymentPlan.getRegularAmount());
        assertEquals(new BigDecimal(3.34).setScale(CURRENCY_PRECISION, DEFAULT_ROUNDING_MODE), paymentPlan.getLastAmount());
        assertTrue(paymentPlan.isLastAmountDifferent());
    }

    @Test
    public void whenValueWithHighAmountOfPayments_ThenCalculatorWorksOutValuesCorrectly() {
        PaymentPlan paymentPlan = PaymentPlanCalculator.calculate(5444333222.00, 1234);
        assertEquals(new BigDecimal(4411939.40).setScale(CURRENCY_PRECISION, DEFAULT_ROUNDING_MODE), paymentPlan.getRegularAmount());
        assertEquals(new BigDecimal(4411941.80).setScale(CURRENCY_PRECISION, DEFAULT_ROUNDING_MODE), paymentPlan.getLastAmount());
        assertTrue(paymentPlan.isLastAmountDifferent());
    }
}